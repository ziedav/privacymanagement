from django.contrib import admin
from .models import Post, Permissions, PermissionSettings,ServiceProvider, Service,ServicePermission,PermissionAccess
# Register your models here.
admin.site.register(Post)
admin.site.register(Permissions)
admin.site.register(PermissionSettings)
admin.site.register(ServiceProvider)
admin.site.register(Service)
admin.site.register(ServicePermission)
admin.site.register(PermissionAccess)
