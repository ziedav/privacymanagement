from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Demo class.
class Post(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

class Permissions(models.Model):
    permissionName = models.CharField(max_length=200)
    description = models.TextField()
    category = models.CharField(max_length=200)
    child = models.CharField(max_length=200, default=False)
    icon = models.CharField(max_length=200, default='contacts')

    def __str__(self):
        return self.permissionName


class PermissionSettings(models.Model):
    permID = models.ForeignKey('Permissions')
    userID = models.ForeignKey('auth.User')
    value = models.BooleanField()
    manual = models.BooleanField()
    lastchanged = models.DateTimeField(default=timezone.now)

    class Meta:
        unique_together = ('permID', 'userID',)

    def __str__(self):
        output = "User %s has shared %s with value %s and category %s" % (self.userID,self.permID, self.value, self.permID.category)
        return output

class ServiceProvider(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    image = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.name

class Service(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    offeredBy = models.ForeignKey('ServiceProvider')
    image = models.ImageField(upload_to='images/')
    availableFrom = models.DateField(default=timezone.now)

    def __str__(self):
        return self.name

class ServicePermission(models.Model):
    service = models.ForeignKey('Service')
    permission = models.ForeignKey('Permissions')
    optional = models.BooleanField(default=False)
    usageInfo = models.TextField()

    def __str__(self):
        return str(self.service) + str(self.permission)

class PermissionAccess(models.Model):
    user = models.ForeignKey('auth.User')
    permission = models.ForeignKey('Permissions')
    service = models.ForeignKey('Service')
    timestamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.user) + str(self.service) + str(self.permission)
