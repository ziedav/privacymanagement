# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-08 15:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ui', '0003_permissions_child'),
    ]

    operations = [
        migrations.AddField(
            model_name='permissions',
            name='icon',
            field=models.CharField(default='contacts', max_length=200),
        ),
    ]
