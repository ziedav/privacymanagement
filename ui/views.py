from django.shortcuts import render, redirect
from .models import Post, Permissions, PermissionSettings, ServiceProvider, Service, ServicePermission, PermissionAccess
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.conf import settings
from django.http.response import HttpResponse
from ml.models import IUIPC
from ml.helpers.helper import Helper
import time


# Create your views here.
def dashboard(request):
    if not request.user.is_authenticated() or request.user.is_anonymous() :
        # print(request.user.is_anonymous())
        return redirect('loginView')

    permissions = Permissions.objects.all()
    # personalData = Permissions.objects.filter(category='Personal Data')
    personalData = PermissionSettings.objects.filter(userID=request.user).filter(permID__category='Personal Data')
    # print(personalData)
    locationData = PermissionSettings.objects.filter(userID=request.user).filter(permID__category='Location Data')
    shoppingData = PermissionSettings.objects.filter(userID=request.user).filter(permID__category='Shopping Data')
    settings = PermissionSettings.objects.filter(userID=request.user)
    recentUsage = PermissionAccess.objects.filter(user=request.user)
    return render(request, 'ui/dashboard.html', {
        'permissions': permissions,
        'personalData': personalData,
        'locationData': locationData,
        'shoppingData': shoppingData,
        'recentUsage': recentUsage,
        'settings' : settings
    })

def settingsView(request):
    if not request.user.is_authenticated() or request.user.is_anonymous() :
        print(request.user.is_anonymous())
        return redirect('loginView')

    return render(request,'ui/settings.html')

def servicesView(request):
    if not request.user.is_authenticated() or request.user.is_anonymous() :
        print(request.user.is_anonymous())
        return redirect('loginView')

    services = Service.objects.all()
    providers = ServiceProvider.objects.all()
    permissions = ServicePermission.objects.all()

    return render(request, 'ui/services.html', {
        'services': services,
        'providers': providers,
        'permissions': permissions
    })

def firstVisit(request):
    # Initially set the permissions to all true, use the Helper Class for this one
    helperinstance = Helper()
    result = helperinstance.setInitialPermissions(request.user)
    if result:
        return render(request, 'ui/welcome_loading.html')


def runPrivacyWizard(request):
    print("TODO")

def loginView(request):
    return render(request, 'ui/login.html', {
        'loginFailed': False
    })

def loginFailed(request):
    return render(request, 'ui/login.html', {
        'loginFailed': True
    })

def doLogin(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)

    if user is not None:
        firstLogin = (user.last_login is None)
        if firstLogin:
            login(request, user)
            return redirect('firstVisit')
        login(request, user)
        return redirect('dashboard')
    else:
        return redirect('loginFailed')

def doLogout(request):
    logout(request)
    return redirect('loginView')

def incomingPermissions(request):
    jsonSettings = request.POST['json']
    print("Received the following data:")
    print(str(jsonSettings))
    helperinstance = Helper()
    res = helperinstance.lookForChanges(jsonSettings, request.user)
    return HttpResponse('SUCCESS')


def incomingIUIPC(request):
    question = request.POST['que']
    answer = request.POST['ans']
    print("Received the following data:")
    print("The question is %s and the answer is %s" % (question, answer))
    helperinstance = Helper()
    res = helperinstance.saveIUIPCResults(question, answer, request.user)
    return HttpResponse('SUCCESS')
