from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.dashboard, name='dashboard'),
    url(r'^services/$', views.servicesView, name='servicesView'),
    url(r'^settings/$', views.settingsView, name='settingsView'),
    url(r'^login/$', views.loginView, name='loginView'),
    url(r'^logout/$', views.doLogout, name='doLogout'),
    url(r'^doLogin/$', views.doLogin, name='doLogin'),
    url(r'^loginFailed/$', views.loginFailed, name='loginFailed'),
    url(r'^loginWelcome/$', views.firstVisit, name='firstVisit'),
    url(r'^post/Permissions/$', views.incomingPermissions, name='incomingPermissions'),
    url(r'^post/IUIPC/$', views.incomingIUIPC, name='incomingIUIPC'),
]
