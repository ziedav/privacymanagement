$(document).ready(function () {

  checkIfDisable();
  checkIfDisableCard();
  $('.superiorPermission').click(function() {
    checkIfDisable();
  });
  $('.ParentCard').click(function() {
    checkIfDisableCard();
  });

  $('#switchCardView').click(function() {
    $('#switchView').html('<i class="material-icons left">view_week</i>Card View<span class="caret"></span><i class="material-icons right">arrow_drop_down</i>');
    $('#switchProfView').removeClass('active');
    $('#switchCardView').addClass('active');

  });
  $('#switchProfView').click(function() {
    $('#switchView').html('<i class="material-icons left">view_week</i>Professional View<span class="caret"></span><i class="material-icons right">arrow_drop_down</i>');
    $('#switchCardView').removeClass('active');
    $('#switchProfView').addClass('active');
  });

  $('#buttonSavePersonal').click(function(){
    var jsonTarget = [];
    $("#personalDataEdit input").each(function() {
      // console.log(this.id)
      var item = {
          id: this.id,
          value: this.checked
        };
      jsonTarget.push(item);
    });
    // console.log(jsonTarget);
    sendPermissions(jsonTarget);
  });
  $('#buttonSaveLocation').click(function(){
    var jsonTarget = [];
    $("#locationDataEdit input").each(function() {
      // console.log(this.id)
      var item = {
          id: this.id,
          value: this.checked
        };
      jsonTarget.push(item);
    });
    // console.log(jsonTarget);
    sendPermissions(jsonTarget);
  });
  $('#buttonSaveShopping').click(function(){
    var jsonTarget = [];
    $("#shoppingDataEdit input").each(function() {
      // console.log(this.id)
      var item = {
          id: this.id,
          value: this.checked
        };
      jsonTarget.push(item);
    });
    // console.log(jsonTarget);
    sendPermissions(jsonTarget);
  });

  $('#buttonSaveProf').click(function(){
    var jsonTarget = [];
    $("#ProfDataEdit input").each(function() {
      // console.log(this.id)
      var item = {
          id: this.id,
          value: this.checked
        };
      jsonTarget.push(item);
    });
    // console.log(jsonTarget);
    sendPermissions(jsonTarget);
  });
});

function checkIfDisable() {
  var MajPerm = $('.superiorPermission').prop('checked');
  console.log(MajPerm);
  if (MajPerm == true) {
    $('.childPermission').each(function() {
      this.disabled = true;
    })
  }
  else {
    $('.childPermission').each(function() {
      this.disabled = false;
    })
  }
};

function checkIfDisableCard() {
  var MajPerm = $('.ParentCard').prop('checked');
  console.log(MajPerm);
  if (MajPerm == true) {
    $('.ChildCard').each(function() {
      this.disabled = true;
    })
  }
  else {
    $('.ChildCard').each(function() {
      this.disabled = false;
    })
  }
};



function sendPermissions(data) {
  $.ajaxSetup({
      beforeSend: function(xhr, settings) {
          if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", csrftoken);
          }
      }
  });
  // console.log(JSON.stringify(data))
  $.post("/post/Permissions/", {json:JSON.stringify(data)} , function(result){
       location.reload();
   });
};

function saveAnswer(question, answer) {

  $.ajaxSetup({
      beforeSend: function(xhr, settings) {
          if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", csrftoken);
          }
      }
  });
  // console.log(JSON.stringify(data))
  $.post("/post/IUIPC/", {que: question, ans: answer} , function(result){
       //
   });

    console.log(question);
    console.log(answer);
}

function checkSurveyCompleted() {
    $('#calcInfoSpinner').removeClass('hidden');
    $('#calcInfoSurvey').removeClass('hidden');
    $('#calcWizard').addClass('hidden');
    // Initiate the calculation of the Regression now
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    // console.log(JSON.stringify(data))
    $.post("/ml/getSurveyPrediction/", {} , function(result){
         location.reload();
     });
};

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
};
