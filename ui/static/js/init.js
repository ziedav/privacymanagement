(function ($) {
    $(function () {

        $('.button-collapse').sideNav();

    }); // end of document ready
})(jQuery); // end of jQuery name space

$(document).ready(function () {
    $(".dropdown-button").dropdown();

    $('.modal-trigger').leanModal();

    //    $('.carousel.carousel-slider').carousel({
    //        full_width: true
    //    });

    // $('.activesurvey input').on('change', function () {
    //     console.log(this);
    //     var question = $('.activesurvey').attr('id');
    //     //        var answer = $('input[name=answer]:checked', '.activesurvey').attr('id');
    //     var answer = this.name;
    //     saveAnswer(question, answer);
    //     privacyWizardNext();
    //     privacyWizardButtonCheck();
    // });

    $('#iuipc1 input').on('change', function () {
        var question = 'iuipc1';
        //        var answer = $('input[name=answer]:checked', '.activesurvey').attr('id');
        var answer = $('#iuipc1 input[name=answer1]:checked').attr('id');
        answer = answer.split("_");
        answer = answer[2]
        saveAnswer(question, answer);
        privacyWizardNext();
        privacyWizardButtonCheck();
    });
    $('#iuipc2 input').on('change', function () {
        var question = 'iuipc2';
        //        var answer = $('input[name=answer]:checked', '.activesurvey').attr('id');
        var answer = $('#iuipc2 input[name=answer2]:checked').attr('id');
        answer = answer.split("_");
        answer = answer[2]
        saveAnswer(question, answer);
        privacyWizardNext();
        privacyWizardButtonCheck();
    });
    $('#iuipc3 input').on('change', function () {
        var question = 'iuipc3';
        //        var answer = $('input[name=answer]:checked', '.activesurvey').attr('id');
        var answer = $('#iuipc3 input[name=answer3]:checked').attr('id');
        answer = answer.split("_");
        answer = answer[2]
        saveAnswer(question, answer);
        privacyWizardNext();
        privacyWizardButtonCheck();
    });
    $('#iuipc4 input').on('change', function () {
        var question = 'iuipc4';
        //        var answer = $('input[name=answer]:checked', '.activesurvey').attr('id');
        var answer = $('#iuipc4 input[name=answer4]:checked').attr('id');
        answer = answer.split("_");
        answer = answer[2]
        saveAnswer(question, answer);
        privacyWizardNext();
        privacyWizardButtonCheck();
    });
    $('#iuipc5 input').on('change', function () {
        var question = 'iuipc5';
        //        var answer = $('input[name=answer]:checked', '.activesurvey').attr('id');
        var answer = $('#iuipc5 input[name=answer5]:checked').attr('id');
        answer = answer.split("_");
        answer = answer[2]
        saveAnswer(question, answer);
        privacyWizardNext();
        privacyWizardButtonCheck();
    });
    $('#iuipc6 input').on('change', function () {
        var question = 'iuipc6';
        //        var answer = $('input[name=answer]:checked', '.activesurvey').attr('id');
        var answer = $('#iuipc6 input[name=answer6]:checked').attr('id');
        answer = answer.split("_");
        answer = answer[2]
        saveAnswer(question, answer);
        privacyWizardNext();
        privacyWizardButtonCheck();
    });
    $('#iuipc7 input').on('change', function () {
        var question = 'iuipc7';
        //        var answer = $('input[name=answer]:checked', '.activesurvey').attr('id');
        var answer = $('#iuipc7 input[name=answer7]:checked').attr('id');
        answer = answer.split("_");
        answer = answer[2]
        saveAnswer(question, answer);
        privacyWizardNext();
        privacyWizardButtonCheck();
    });
    $('#iuipc8 input').on('change', function () {
        var question = 'iuipc8';
        //        var answer = $('input[name=answer]:checked', '.activesurvey').attr('id');
        var answer = $('#iuipc8 input[name=answer8]:checked').attr('id');
        answer = answer.split("_");
        answer = answer[2]
        saveAnswer(question, answer);
        privacyWizardNext();
        privacyWizardButtonCheck();
    });
    $('#iuipc9 input').on('change', function () {
        var question = 'iuipc9';
        //        var answer = $('input[name=answer]:checked', '.activesurvey').attr('id');
        var answer = $('#iuipc9 input[name=answer9]:checked').attr('id');
        answer = answer.split("_");
        answer = answer[2]
        saveAnswer(question, answer);
        privacyWizardNext();
        privacyWizardButtonCheck();
    });
    $('#iuipc10 input').on('change', function () {
        var question = 'iuipc10';
        //        var answer = $('input[name=answer]:checked', '.activesurvey').attr('id');
        var answer = $('#iuipc10 input[name=answer10]:checked').attr('id');
        answer = answer.split("_");
        answer = answer[2]
        saveAnswer(question, answer);
        privacyWizardNext();
        privacyWizardButtonCheck();
    });



});

var carousel_at = 1;


function buttonPrivacyWizard() {
    $("#privacyWizard").openModal();
    loadPrivacyWizard();
};

function loadPrivacyWizard() {
    $('.carousel.carousel-slider').carousel({
        full_width: true,
        no_wrap: true,
        indicators: false,
        time_constant: 20
    });
    $('#privacystart').hide();
    privacyWizardButtonCheck();
};

function privacyWizardNext() {
    $('.carousel').carousel('next');
    carousel_at += 1;
    privacyWizardButtonCheck();
};

function privacyWizardPrevious() {
    $('.carousel').carousel('prev');
    carousel_at -= 1;
    privacyWizardButtonCheck();
};

function privacyWizardButtonCheck() {
    if (carousel_at == 1) {
        $('#PrivBack').hide();
    } else if (carousel_at == 11) {
        $('#PrivNext').hide();
    } else {
        $('#PrivBack').show();
        $('#PrivNext').show();
    }
};
