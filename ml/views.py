from django.shortcuts import render, redirect
from django.http.response import HttpResponse
from django.contrib.auth.models import User
import csv
import random


from collections import Counter
import numpy as np
import matplotlib.pyplot as plt

from sklearn import cluster, ensemble, cross_validation, svm, linear_model, metrics
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error
from sklearn.svm import SVC
from sklearn.cross_validation import StratifiedKFold
from sklearn.feature_selection import RFE,RFECV
import pickle

from ml.preprocessing import csvimporter
from ml.helpers.helper import Helper
from ml.models import IUIPC, Baseline
# Create your views here.

def createMainClassifier(request):
    # return "TODO"

    Targets = ['sharelikeability_0','sharelikeability_1','sharelikeability_2','sharelikeability_3','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    'sharelikeability_8','sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']
    TargetLabels = ['name', 'birthday', 'address', 'allergies', 'recent_shop_visits', 'nutrition_preferences', 'only_the_product_categories', 'household_income',
    'article_wishlist', 'device_location', 'products_as_on_receipt', 'recently_viewed_articles', 'loyalty_points', 'only_the_price', 'only_the_amount', 'in-store_location']

    for i in range(16):
        csv = csvimporter.Importer()
        Target = Targets[i]
        Features = ['IUIPCCollection','IUIPCControl','IUIPCAwareness']
        (data, target) = csv.importCSV(Target, Features)
        vc = linear_model.Ridge(alpha=1, normalize=False, solver='cholesky')
        # vc = svm.SVR(kernel='linear')
        vc = vc.fit(data, target)
        # save the model to disk
        filename = 'estimators/' + str(TargetLabels[i]) + '.sv'
        pickle.dump(vc, open(filename, 'wb'))

    # loaded_model = pickle.load(open(filename, 'rb'))

    return HttpResponse('Successfully trained %d targets' % len(Targets))

def createBaselineClassifier(request):
    Targets = ['sharelikeability_0','sharelikeability_1','sharelikeability_2','sharelikeability_3','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    'sharelikeability_8','sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']
    TargetLabels = ['name', 'birthday', 'address', 'allergies', 'recent_shop_visits', 'nutrition_preferences', 'only_the_product_categories', 'household_income',
    'article_wishlist', 'device_location', 'products_as_on_receipt', 'recently_viewed_articles', 'loyalty_points', 'only_the_price', 'only_the_amount', 'in-store_location']
    baselineClassifier = {}
    for i in range(16):
        csv = csvimporter.Importer()
        Target = Targets[i]
        Features = ['IUIPCCollection','IUIPCControl','IUIPCAwareness']
        (data, target) = csv.importCSV(Target, Features)

        length = len(data)
        baselineClassifier['len'] = length
        # print(length)
        counter = 0
        for item in target:
            if (item < 3.5):
                counter = counter + 0
            else:
                counter = counter + 1
        probability = (counter / length)
        baselineClassifier[TargetLabels[i]] = probability
        # print(probability)
    filename = 'estimators/baselineClassifier.sv'
    print(baselineClassifier)
    pickle.dump(baselineClassifier, open(filename, 'wb'))
    return HttpResponse('Successfully trained %d targets' % len(Targets))

def getBaselinePrediction(request):
    if not request.user.is_authenticated():
        return redirect('/login/')

    TargetLabels = ['name', 'birthday', 'address', 'allergies', 'recent_shop_visits', 'nutrition_preferences', 'only_the_product_categories', 'household_income',
    'article_wishlist', 'device_location', 'products_as_on_receipt', 'recently_viewed_articles', 'loyalty_points', 'only_the_price', 'only_the_amount', 'in-store_location']
    return_string = ""
    filename = 'estimators/baselineClassifier.sv'
    baselineClassifier = pickle.load(open(filename, 'rb'))
    for i in range(16):
        Target = TargetLabels[i]
        probability = baselineClassifier[Target]
        predicted = np.random.choice(np.arange(3,5), 1, p=[(1-probability), probability])
        helperinstance = Helper()
        predicted = helperinstance.BinaryToBoolean(predicted[0])
        return_string = return_string + "<p>" + str(TargetLabels[i]) + " should be set to: " + str(predicted) + " based on the probability of " + str(probability) + "</p><br/>"
        # print(return_string)

        helperinstance.savePrediction(request.user, TargetLabels[i], predicted)
    return HttpResponse("SUCCESS")
# Todo: Add features as input
def getSurveyPrediction(request):
    if not request.user.is_authenticated():
        return redirect('/login/')
    baseline = Baseline.objects.get(UserID=request.user)
    if baseline.usesBaseline == True:
        return redirect('/ml/getBaselinePrediction')
    # username = request.user
    iuipc_results = IUIPC.objects.get(UserID=request.user)
    # print(iuipc_results)
    iuipc_collection = iuipc_results.getCollection()
    iuipc_control = iuipc_results.getControl()
    iuipc_awareness = iuipc_results.getAwareness()
    inputFeatures = [iuipc_collection, iuipc_control, iuipc_awareness]
    inputFeatures = np.asarray(inputFeatures)
    inputFeatures = inputFeatures.reshape(1, -1)
    Targets = ['sharelikeability_0','sharelikeability_1','sharelikeability_2','sharelikeability_3','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    'sharelikeability_8','sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']
    TargetLabels = ['name', 'birthday', 'address', 'allergies', 'recent_shop_visits', 'nutrition_preferences', 'only_the_product_categories', 'household_income',
    'article_wishlist', 'device_location', 'products_as_on_receipt', 'recently_viewed_articles', 'loyalty_points', 'only_the_price', 'only_the_amount', 'in-store_location']
    return_string = "<h1>Your results</h1>: <p>You were using the following features: "
    return_string = return_string + str(iuipc_results) + " </p>"
    for i in range(16):
        filename = 'estimators/' + str(TargetLabels[i]) + '.sv'
        loaded_model = pickle.load(open(filename, 'rb'))
        result = loaded_model.predict(inputFeatures)
        helperinstance = Helper()
        result_bool = helperinstance.BinaryToBoolean(result)
        return_string = return_string + "<p>" + str(TargetLabels[i]) + " should be set to: " + str(result) + " which equals to a setting of: " + str(result_bool) + "</p><br/>"
        # print(TargetLabels[i])
        # print(result)
        # Try to save to databases
        helperinstance.savePrediction(request.user, TargetLabels[i], result_bool)
    # return HttpResponse(return_string)
    return HttpResponse("SUCCESS")

def testInitialPermissions(request):
    if not request.user.is_authenticated():
        return redirect('/login/')

    helperinstance = Helper()
    result = helperinstance.setInitialPermissions(request.user)
    return HttpResponse("See log for details")
def saveClassifier(request):
    return "TODO"

def loadClassifier(request):
    return "TODO"
