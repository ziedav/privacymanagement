from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from sklearn import cross_validation
from sklearn.linear_model import LogisticRegression
import pickle

# Create your models here.
class IUIPC(models.Model):
    UserID = models.ForeignKey('auth.User')
    iuipc1=models.IntegerField(default=1)
    iuipc2=models.IntegerField(default=1)
    iuipc3=models.IntegerField(default=1)
    iuipc4=models.IntegerField(default=1)
    iuipc5=models.IntegerField(default=1)
    iuipc6=models.IntegerField(default=1)
    iuipc7=models.IntegerField(default=1)
    iuipc8=models.IntegerField(default=1)
    iuipc9=models.IntegerField(default=1)
    iuipc10=models.IntegerField(default=1)

    def getCollection(self):
        return (self.iuipc1+self.iuipc2+self.iuipc3+self.iuipc4)/4.0
    def getControl(self):
        return (self.iuipc5+self.iuipc6+self.iuipc7)/3.0
    def getAwareness(self):
        return (self.iuipc8+self.iuipc9+self.iuipc10)/3.0

    def __str__(self):
        ret = "Collection score for user %s: " % self.UserID
        ret = ret + str(round(self.getCollection(),2))
        ret = ret + " Control score: "
        ret = ret + str(round(self.getControl(),2))
        ret = ret + " Awareness score: "
        ret = ret + str(round(self.getAwareness(),2))
        return ret

class Baseline(models.Model):
    UserID = models.OneToOneField('auth.User')
    usesBaseline = models.BooleanField(default=False)

    def __str__(self):
        ret = "User %s " % self.UserID
        ret = ret + ": %s" % self.usesBaseline
        return ret
