from django.contrib import admin
from .models import IUIPC, Baseline
# Register your models here.
admin.site.register(IUIPC)
admin.site.register(Baseline)
