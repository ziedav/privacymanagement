#!/usr/bin/python
import csv
import random

from collections import Counter
import numpy as np
import matplotlib.pyplot as plt

from sklearn import cluster, ensemble, cross_validation, svm, linear_model, metrics
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error
from sklearn.svm import SVC
from sklearn.cross_validation import StratifiedKFold
from sklearn.feature_selection import RFE,RFECV

maxs_results = []

def checkRow(row):
    sharelikeability_name =               row['sharelikeability_0']
    sharelikeability_birthday =               row['sharelikeability_1']
    sharelikeability_address =               row['sharelikeability_2']
    sharelikeability_allergies =               row['sharelikeability_3']
    sharelikeability_recent_shop_visits =               row['sharelikeability_4']
    sharelikeability_nutrition_preferences =               row['sharelikeability_5']
    sharelikeability_only_product_categories =               row['sharelikeability_6']
    sharelikeability_household_income =               row['sharelikeability_7']
    sharelikeability_article_wishlist =               row['sharelikeability_8']
    sharelikeability_location_device =               row['sharelikeability_9']
    sharelikeability_bought_products_receipt =               row['sharelikeability_10']
    sharelikeability_recently_viewed =               row['sharelikeability_11']
    sharelikeability_loyalty_points =               row['sharelikeability_12']
    sharelikeability_only_price =               row['sharelikeability_13']
    sharelikeability_only_amount =               row['sharelikeability_14']
    sharelikeability_location_store =               row['sharelikeability_15']

    targetitem = [sharelikeability_name,sharelikeability_birthday,sharelikeability_address,sharelikeability_allergies,sharelikeability_recent_shop_visits,sharelikeability_nutrition_preferences,
    sharelikeability_only_product_categories,sharelikeability_household_income,sharelikeability_article_wishlist,sharelikeability_location_device,sharelikeability_bought_products_receipt,
    sharelikeability_recently_viewed,sharelikeability_loyalty_points,sharelikeability_only_price,sharelikeability_only_amount,sharelikeability_location_store]

    targetitem = [item.replace(",", ".") for item in targetitem]
    targetitem =  [round(float(i),2) for i in targetitem]

    restrict = 0
    allow = 0
    for i in targetitem:
        if (i < 3.5):
            restrict = restrict + 1
        else:
            allow = allow + 1

    if (restrict == 0) or (allow == 0):
        # print(targetitem)
        # print("Trivial case")
        # print(restrict, allow)
        return True

    # Testing
    # if (restrict < 3) or (allow < 3):
    #     return True
    return False

def readRow(row, Target = None, Features = []):
    Gender = row['Gender']
    Age =               row['Age']
    country =            row['country']
    education =               row['education']
    incomecategory =            row['incomecategory']
    privacyinvasion =               row['privacyinvasion']
    Deviceusage_smartphone =            row['Deviceusage_smartphone']
    Deviceusage_desktop =               row['Deviceusage_desktop']
    Deviceusage_laptop =            row['Deviceusage_laptop']
    Deviceusage_tablet =               row['Deviceusage_tablet']
    Deviceusage_smartwatch =            row['Deviceusage_smartwatch']
    Deviceusage_mobilephone =               row['Deviceusage_mobilephone']
    falsifiedinformation =            row['falsifiedinformation']
    IUIPCCollection =                        row['IUIPCCollection']
    IUIPCControl =                        row['IUIPCControl']
    IUIPCAwareness =                        row['IUIPCAwareness']
    bf_extraversion =                   row['Extraversion']
    bf_agreeableness =                   row['Agreeableness']
    bf_conscientiousness =                   row['Conscientiousness']
    bf_neuroticism =                   row['Neuroticism']
    bf_openness =                   row['Openness']
    sharelikeability_name =               row['sharelikeability_0']
    sharelikeability_birthday =               row['sharelikeability_1']
    sharelikeability_address =               row['sharelikeability_2']
    sharelikeability_allergies =               row['sharelikeability_3']
    sharelikeability_recent_shop_visits =               row['sharelikeability_4']
    sharelikeability_nutrition_preferences =               row['sharelikeability_5']
    sharelikeability_only_product_categories =               row['sharelikeability_6']
    sharelikeability_household_income =               row['sharelikeability_7']
    sharelikeability_article_wishlist =               row['sharelikeability_8']
    sharelikeability_location_device =               row['sharelikeability_9']
    sharelikeability_bought_products_receipt =               row['sharelikeability_10']
    sharelikeability_recently_viewed =               row['sharelikeability_11']
    sharelikeability_loyalty_points =               row['sharelikeability_12']
    sharelikeability_only_price =               row['sharelikeability_13']
    sharelikeability_only_amount =               row['sharelikeability_14']
    sharelikeability_location_store =               row['sharelikeability_15']

    # Check if trivial = 16 trivial cases in this data set
    if checkRow(row):
        return (None, None)

    if len(Features) >= 1:
        dataitem = []
        for feature in Features:
            dataitem.append(row[feature])
    else:
        dataitem = [Gender,Age,country,education,incomecategory,privacyinvasion,Deviceusage_smartphone,Deviceusage_desktop,Deviceusage_laptop,Deviceusage_tablet,Deviceusage_smartwatch,Deviceusage_mobilephone,
        falsifiedinformation,IUIPCCollection,IUIPCControl,IUIPCAwareness,bf_extraversion,bf_agreeableness,bf_conscientiousness,
        bf_neuroticism,bf_openness]

    if (Target != None):
        targetitem = [row[Target]]
    else:
        targetitem = [sharelikeability_name,sharelikeability_birthday,sharelikeability_address,sharelikeability_allergies,sharelikeability_recent_shop_visits,sharelikeability_nutrition_preferences,
        sharelikeability_only_product_categories,sharelikeability_household_income,sharelikeability_article_wishlist,sharelikeability_location_device,sharelikeability_bought_products_receipt,
        sharelikeability_recently_viewed,sharelikeability_loyalty_points,sharelikeability_only_price,sharelikeability_only_amount,sharelikeability_location_store]

    #Replace , with . to convert it to float later
    dataitem = [item.replace(",", ".") for item in dataitem]
    #Convert list to float
    dataitem =  [round(float(i),2) for i in dataitem]
    #Replace , with . to convert it to float later
    targetitem = [item.replace(",", ".") for item in targetitem]
    targetitem =  [round(float(i),2) for i in targetitem]

    # print(len(targetitem))
    # print(dataitem)

    return (dataitem, targetitem)

def scaleToBinaryBool (value):
    # print("The following should be interpreted %s" % value)
    value = float(value)
    if (value < 3.5):
        return False
    else:
        return True

def evaluateClass (estdata, testdata, baseline=False):
    # print(estdata)
    # Remove the valuse for the targets and set them to 0
    correct = 0
    wrong = 0
    total = 0
    total = len(estdata)
    # print(estdata)
    if (baseline == False):
        for i in range(total):
            if scaleToBinaryBool(estdata[i]) == scaleToBinaryBool(testdata[i]):
                correct = correct + 1
            else:
                wrong = wrong + 1

        # print(estdata)
        # print(testdata)
        return (correct/total)

    # Assume everything is set to share initially
    else:
        for i in range(total):
            if testdata[i]:
                correct = correct + 1
            else:
                wrong = wrong + 1
        return (correct/total)

# def shuffleCSV(size_1, size_2, specTarget = None, featuresToUse = [], random_state = 0):
# Split: e.g. 0.10
def shuffleCSV(split, specTarget = None, featuresToUse = [], random_state = 0):
    training_data = []
    training_target = []
    test_data = []
    test_target = []
    with open('learning_data.csv', newline='') as csvfile:
        dictreader = csv.DictReader(csvfile, delimiter=';', quotechar='|')
        # dictreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        shuffleing = list(range(103))
        # print(shuffleing)
        rs = np.random.RandomState(random_state)
        # rs.shuffle(shuffleing)
        # random.shuffle(shuffleing)
        # print(shuffleing)
        # index = 0
        # training = 0
        # test = 0
        # for row in dictreader:
        #     (item_data,item_target) = readRow(row,specTarget,featuresToUse)
        #     if (item_data != None):
        #         if (((test * split)) >= training):
        #             # (item_data,item_target) = readRow(row,'sharelikeability_7',['Deviceusage_smartwatch'])
        #                 training_data.append(item_data)
        #                 training_target.append(item_target)
        #                 training = training + 1
        #
        #         else:
        #             # (item_data,item_target) = readRow(row,'sharelikeability_7',['Deviceusage_smartwatch'])
        #                 test_data.append(item_data)
        #                 test_target.append(item_target)
        #                 test = test + 1
        #
        #     index = index + 1
        data = []
        target = []
        for row in dictreader:
            (item_data,item_target) = readRow(row,specTarget,featuresToUse)
            if (item_data != None):
                        data.append(item_data)
                        target.append(item_target)

        # Now split into test and training
        X_train, X_test, y_train, y_test = cross_validation.train_test_split(data, target, test_size=split, random_state=random_state)
        # print(np.shape(X_train))
        # print(np.shape(X_test))
        # print(np.shape(y_test))
        # print(X_test)
        # print(y_test)


        # print("Training includes %d and Test includes %d" % (training,test))

        # print(training_target)
        # print(training_data)
        # print(test_data)
        # print(test_target)
        # print(test)
        # print(np.shape(training_data))
        # print(np.shape(test_data))
        return (X_train, y_train, X_test, y_test)

def LinSVR (X_train, y_train, X_test, y_test):
    vc = svm.SVR(kernel='linear')
    vc = vc.fit(X_train, y_train)

    expected = y_test
    predicted = vc.predict(X_test)
    expected =  [round(float(i),2) for i in expected]
    # print(expected)
    # print(predicted)
    rating = mean_squared_error(predicted, expected)
    print(rating)

def RidgeRegr (X_train, y_train, X_test, y_test, type = 0):
    vc = linear_model.Ridge(alpha=1, normalize=False, solver='cholesky')
    # vc = svm.SVR(kernel='linear')
    vc = vc.fit(X_train, y_train)
    expected = y_test
    predicted = vc.predict(X_test)
    expected =  [round(float(i),2) for i in expected]
    # print(expected)
    # print(predicted)
    if (type == 1):
        score = evaluateClass(predicted, expected)
        return score
    rating = mean_squared_error(predicted, expected)
    return rating
    # print(rating, score)
    # return rating
    # print(vc.coef_)
    # print(rating)


def Baseline (X_train, y_train, X_test, y_test):
    length = len(X_train)
    # print(length)
    counter = 0
    for item in y_train:
        if (item < 3.5):
            counter = counter + 0
        else:
            counter = counter + 1
    probability = (counter / length)
    # print(probability)

    predicted = np.random.choice(np.arange(3,5), len(y_test), p=[(1-probability), probability])
    expected = y_test
    expected =  [round(float(i),2) for i in expected]
    score = evaluateClass(predicted, expected)
    # print(generated)

    return score

def runRegressionWSS(i):
    all_targets = ['sharelikeability_0','sharelikeability_1','sharelikeability_2','sharelikeability_3','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    'sharelikeability_8','sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']

    # all_targets = ['sharelikeability_0','sharelikeability_2','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    # 'sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']

    scorings = []
    feature_occurences = []
    for target in all_targets:
        # all_features = ['Gender','country','Age','education','incomecategory','privacyinvasion','Deviceusage_smartphone','Deviceusage_desktop','Deviceusage_laptop','Deviceusage_tablet',
        # 'Deviceusage_smartwatch','Deviceusage_mobilephone','falsifiedinformation','IUIPCCollection','IUIPCControl','IUIPCAwareness','Extraversion','Agreeableness','Conscientiousness','Neuroticism','Openness']

        # all_features = ['Gender','country','Age','education','incomecategory','privacyinvasion','Deviceusage_smartphone','Deviceusage_desktop','Deviceusage_laptop','Deviceusage_tablet',
        # 'Deviceusage_smartwatch','Deviceusage_mobilephone','falsifiedinformation','IUIPCCollection','IUIPCControl','IUIPCAwareness','Extraversion',
        # 'Agreeableness','Conscientiousness','Neuroticism','Openness']

        # all_features = ['Extraversion','Agreeableness','Conscientiousness','Neuroticism','Openness']
        all_features = ['Gender','Age','falsifiedinformation','privacyinvasion']
        # all_features = ['IUIPCCollection','IUIPCControl','IUIPCAwareness']

        active_features = []
        feature_scores = []
        while (len(all_features) > 0) and (len(active_features) < 12):
            best_score = 1000.0
            best_feature = None
            for feature in all_features:
                try_features = list(active_features)

                try_features.append(feature)
                # print(try_features)
                # Get the data from the CSV file
                (training_data, training_target, test_data, test_target) = shuffleCSV(0.25, target,featuresToUse = try_features ,random_state = i*100)
                # Split the test data into data for the WSS and for the Evaluation
                X_wss, X_eval, y_wss, y_eval = cross_validation.train_test_split(test_data, test_target, test_size=0.5, random_state=i*100)
                # Make them Numpy Arrays
                training_data = np.asarray(training_data)
                training_target = np.ravel(np.asarray(training_target))
                test_data = np.asarray(X_wss)
                test_target = np.ravel(np.asarray(y_wss))
                # Run scoring
                scoring = RidgeRegr(training_data, training_target, test_data, test_target)

                # print(scoring)
                # If the scoring for this feature(s) is better then update the best_score and set best_feature to the newly chosen one
                if (scoring < best_score):
                    best_score = scoring
                    best_feature = feature
                    if (feature == None):
                        print("Fuck a feature was None")

            # Count for the first 8 chosen features their occurences
            # if (len(active_features) <= 5):
            #     feature_occurences.append(best_feature)
            # print("The best features is %s scoring a score of %.2f" % (best_feature, best_score))
            # Now add the best_feature to the active_features set for the next run and remove it from the all_features
            active_features.append(best_feature)
            try:
                all_features.remove(best_feature)
            except ValueError:
                print("tryed to remove %s " % best_feature)
                print("from")
                print(all_features)
                print("all active features are and the best_score was %.2f" % best_score)
                print(active_features)
            # Save the score for the active_features
            # print(all_features)
            feature_scores.append([float(best_score), list(active_features)])
            # print(feature_scores)
            # print(best_score)
            # print(active_features)
            # print(feature_scores)
            best_score = 1000.0
            best_feature = None
            # print(list(sorted(feature_scores)[0][1]))
        feature_occurences = list(feature_occurences + list(sorted(feature_scores)[0][1]))

        # Use the calculated scores now to get a scores
        #
        #
        features_eval = sorted(feature_scores)[0][1]
        (training_data, training_target, test_data, test_target) = shuffleCSV(0.25, target,featuresToUse = features_eval ,random_state = i*100)
        # Split the test data into data for the WSS and for the Evaluation
        X_wss, X_eval, y_wss, y_eval = cross_validation.train_test_split(test_data, test_target, test_size=0.5, random_state=i*100)
        # Make them Numpy Arrays
        training_data = np.asarray(training_data)
        training_target = np.ravel(np.asarray(training_target))
        test_data = np.asarray(X_eval)
        test_target = np.ravel(np.asarray(y_eval))
        score = RidgeRegr(training_data, training_target, test_data, test_target, 1)
        scorings.append(score)
        # print(features_eval)
        # print("The score for the WSS calculated features is: %.2f while the wss score was %.2f" % (score, sorted(feature_scores)[0][0]))
        #
        #
        #

        # print(feature_occurences)
        # print(sorted(feature_scores)[len(feature_scores)])
        # print("New target now %s" % target)
        # print(feature_occurences)


    # print(Counter(feature_occurences))
    print("This run scored a total score of mean %.2f " % np.mean(scorings))
    return (feature_occurences,scorings)

def runRegressionOnTargets (i):
    all_features = ['IUIPCCollection','IUIPCControl','IUIPCAwareness']
    # all_features = []
    # all_targets = ['sharelikeability_0','sharelikeability_1','sharelikeability_2','sharelikeability_3','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    # 'sharelikeability_8','sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']
    all_targets = ['sharelikeability_13','sharelikeability_11','sharelikeability_9','sharelikeability_8','sharelikeability_4','sharelikeability_3','sharelikeability_1','sharelikeability_7','sharelikeability_5','sharelikeability_12','sharelikeability_2','sharelikeability_0','sharelikeability_6','sharelikeability_14','sharelikeability_10','sharelikeability_15']
    scores_all = []
    while (len(all_targets)-1 > 0):
        # Add the first target in list to the features list
        all_features.append(all_targets[0])
        all_targets.remove(all_targets[0])
        scores = []
        for target in all_targets:

            # print("This assumes that the user confirmed the first %d targets" % len(all_features))
            (training_data, training_target, test_data, test_target) = shuffleCSV(0.20, target, featuresToUse = all_features, random_state = i*100)
            training_data = np.asarray(training_data)
            training_target = np.ravel(np.asarray(training_target))
            test_data = np.asarray(test_data)
            test_target = np.ravel(np.asarray(test_target))
            scoring = RidgeRegr(training_data, training_target, test_data, test_target, 1)
            # print("The score is : %.2f" % scoring)
            scores.append(scoring)
        # print(scores)
        # print(all_features)
        scores_all.append(np.mean(scores))
        # print(all_targets)
        # print(np.mean(scores))
    # print(scores_all)
    return scores_all

def runRegressionWSS_Score(i):
    all_targets = ['sharelikeability_0','sharelikeability_1','sharelikeability_2','sharelikeability_3','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    'sharelikeability_8','sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']
    scorings = []
    feature_occurences = []
    for target in all_targets:
        # all_features = ['Gender','country','Age','education','incomecategory','privacyinvasion','Deviceusage_smartphone','Deviceusage_desktop','Deviceusage_laptop','Deviceusage_tablet',
        # 'Deviceusage_smartwatch','Deviceusage_mobilephone','falsifiedinformation','IUIPCCollection','IUIPCControl','IUIPCAwareness','Extraversion','Agreeableness','Conscientiousness','Neuroticism','Openness']

        all_features = ['Gender','country','Age','education','incomecategory','privacyinvasion','Deviceusage_smartphone','Deviceusage_desktop','Deviceusage_laptop','Deviceusage_tablet',
        'Deviceusage_smartwatch','Deviceusage_mobilephone','falsifiedinformation','IUIPCCollection','IUIPCControl','IUIPCAwareness','Extraversion','Agreeableness','Conscientiousness','Neuroticism','Openness']

        active_features = []
        feature_scores = []
        while (len(all_features) > 0) and (len(active_features) < 12):
            best_score = 0.0
            best_feature = None
            for feature in all_features:
                try_features = list(active_features)

                try_features.append(feature)
                # print(try_features)
                # Get the data from the CSV file
                (training_data, training_target, test_data, test_target) = shuffleCSV(0.25, target,featuresToUse = try_features ,random_state = i*100)
                # Split the test data into data for the WSS and for the Evaluation
                X_wss, X_eval, y_wss, y_eval = cross_validation.train_test_split(test_data, test_target, test_size=0.5, random_state=i*100)
                # Make them Numpy Arrays
                training_data = np.asarray(training_data)
                training_target = np.ravel(np.asarray(training_target))
                test_data = np.asarray(X_wss)
                test_target = np.ravel(np.asarray(y_wss))
                # Run scoring
                scoring = RidgeRegr(training_data, training_target, test_data, test_target)

                # print(scoring)
                # If the scoring for this feature(s) is better then update the best_score and set best_feature to the newly chosen one
                if (scoring > best_score):
                    best_score = scoring
                    best_feature = feature
                    if (feature == None):
                        print("Fuck a feature was None")

            # Count for the first 8 chosen features their occurences
            if (len(active_features) <= 5):
                feature_occurences.append(best_feature)
            print("The best features is %s scoring a score of %.2f" % (best_feature, best_score))
            # Now add the best_feature to the active_features set for the next run and remove it from the all_features
            active_features.append(best_feature)
            try:
                all_features.remove(best_feature)
            except ValueError:
                print("tryed to remove %s " % best_feature)
                print("from")
                print(all_features)
                print("all active features are and the best_score was %.2f" % best_score)
                print(active_features)
            # Save the score for the active_features

            feature_scores.append([float(best_score), list(active_features)])
            # print(feature_scores)
            # print(best_score)
            # print(active_features)
            best_score = 0.0
            best_feature = None
        # print(sorted(feature_scores))
        # print(sorted(feature_scores)[len(feature_scores)])
        print("New target now %s" % target)
        # print(feature_occurences)

    print(Counter(feature_occurences))
    return feature_occurences
def runRegressionSimple (i):
    all_targets = ['sharelikeability_0','sharelikeability_1','sharelikeability_2','sharelikeability_3','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    'sharelikeability_8','sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']
    scorings = []

    all_features = ['Extraversion','Agreeableness','Conscientiousness','Neuroticism','Openness']
    for target in all_targets:

        # (training_data, training_target, test_data, test_target) = shuffleCSV(90,103, target)
        (training_data, training_target, test_data, test_target) = shuffleCSV(0.10, target, all_features, random_state = i*100)
        training_data = np.asarray(training_data)
        training_target = np.ravel(np.asarray(training_target))
        test_data = np.asarray(test_data)
        test_target = np.ravel(np.asarray(test_target))
        scoring = RidgeRegr(training_data, training_target, test_data, test_target, 1)
        scorings.append(scoring)
    # print(scorings)
    return(np.mean(scorings), scorings)

def findEasy ():
    shuffleCSV(90,103, 'sharelikeability_0')
def runRegressionComplex ():
    # all_targets = ['sharelikeability_0','sharelikeability_1','sharelikeability_2','sharelikeability_3','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    # 'sharelikeability_8','sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']

    # # Removed 'sharelikeability_2' => 2.60
    # all_targets = ['sharelikeability_0','sharelikeability_1','sharelikeability_3','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    # 'sharelikeability_8','sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']

    # Removed 'sharelikeability_2', 'sharelikeability_3' => 2.43
    all_targets = ['sharelikeability_0','sharelikeability_1','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    'sharelikeability_8','sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']

    # Removed 'sharelikeability_2', 'sharelikeability_3', 'sharelikeability_0' => 2.16
    all_targets = ['sharelikeability_1','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    'sharelikeability_8','sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']

    # Removed 'sharelikeability_2', 'sharelikeability_3', 'sharelikeability_0' ,'sharelikeability_9' => 1.93
    all_targets = ['sharelikeability_1','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    'sharelikeability_8','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']

    # Removed 'sharelikeability_2', 'sharelikeability_3', 'sharelikeability_0' ,'sharelikeability_9','sharelikeability_7' => 1.76
    all_targets = ['sharelikeability_1','sharelikeability_4','sharelikeability_5','sharelikeability_6',
    'sharelikeability_8','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']

    # Removed 'sharelikeability_2', 'sharelikeability_3', 'sharelikeability_0' ,'sharelikeability_9','sharelikeability_7', 'sharelikeability_1' => 1.57
    all_targets = ['sharelikeability_4','sharelikeability_5','sharelikeability_6',
    'sharelikeability_8','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']

    # Removed 'sharelikeability_2', 'sharelikeability_3', 'sharelikeability_0' ,'sharelikeability_9','sharelikeability_7', 'sharelikeability_1' , 'sharelikeability_8',
    all_targets = ['sharelikeability_4','sharelikeability_5','sharelikeability_6',
    'sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']

    at_target = 0
    counter = [0,0,0,0,0,0,0,0,0]
    for target in all_targets:
        t_rating = 0.0
        (training_data, training_target, test_data, test_target) = shuffleCSV(90,103, target)
        training_data = np.asarray(training_data)
        training_target = np.ravel(np.asarray(training_target))
        test_data = np.asarray(test_data)
        test_target = np.ravel(np.asarray(test_target))
        t_rating = RidgeRegr(training_data, training_target, test_data, test_target)
        prev_rating = float(counter[at_target])
        # print(at_target)
        counter[at_target] = prev_rating + t_rating
        at_target = at_target + 1
    # print(counter)
    return counter
    # print(test_target)

def calculateBaseline (i):
    all_targets = ['sharelikeability_0','sharelikeability_1','sharelikeability_2','sharelikeability_3','sharelikeability_4','sharelikeability_5','sharelikeability_6','sharelikeability_7',
    'sharelikeability_8','sharelikeability_9','sharelikeability_10','sharelikeability_11','sharelikeability_12','sharelikeability_13','sharelikeability_14','sharelikeability_15']
    scorings = []
    for target in all_targets:
        (training_data, training_target, test_data, test_target) = shuffleCSV(0.25, target, random_state = i*100)
        # Split the test data into data for the WSS and for the Evaluation
        X_wss, X_eval, y_wss, y_eval = cross_validation.train_test_split(test_data, test_target, test_size=0.5, random_state=i*100)
        training_data = np.asarray(training_data)
        training_target = np.ravel(np.asarray(training_target))
        test_data = np.asarray(X_eval)
        test_target = np.ravel(np.asarray(y_eval))
        scoring = Baseline(training_data, training_target, test_data, test_target)
        scorings.append(scoring)
    # print(scorings)
    return(np.mean(scorings), scorings)


# def runBase():
#     results = []
#     all_counters = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
#     for i in range(100):
#         (temp,scorings) = calculateBaseline(i)
#         results.append(temp)
#         for x in range(len(scorings)):
#             all_counters[x] = all_counters[x] + scorings[x]
#     all_counters =  [round(float(i/100),2) for i in all_counters]
#     # print(results)
#     print("Mean is %.2f" % np.mean(results))
#     print(all_counters)

def runBase():
    print("Running the Baseline")
    ts = []
    for i in range(10):
        print("Run %d of 10" % i)
        (score, scs) = calculateBaseline(i)
        ts.append(scs)
    print(ts)
    print(np.mean(ts))

# Without 8 and 3
def runInterChanged():
    results = []
    all_counters = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    for i in range(100):
        (temp,scorings) = runRegressionSimple(i)
        results.append(temp)
        for x in range(len(scorings)):
            all_counters[x] = all_counters[x] + scorings[x]
    all_counters =  [round(float(i/100),2) for i in all_counters]
    # print(results)
    print("Mean is %.2f" % np.mean(results))
    print(all_counters)

def runWSS():
    print("Running Ridge Regression with WSS now")
    feature_occurences = []
    target_scores = []
    mean_scores = []
    for i in range(10):
        print("Run %d of 10" % i)
        (occurences, scores) = runRegressionWSS(i)
        feature_occurences = feature_occurences + occurences
        target_scores.append(scores)
    print(Counter(feature_occurences))
    print(target_scores)

    print(np.mean(target_scores))

def runWithoutWSS():
    target_scores = []
    for i in range(10):
        print("Run %d of 10" % i)
        (score, scorings) = runRegressionSimple(i)
        target_scores.append(scorings)
    print(scorings)
    print(np.mean(scorings))
# runWSS()
# runBase()
# runWithoutWSS()
scos = []
for i in range(100):
    sco = runRegressionOnTargets(i)
    scos.append(sco)
print(scos)
print(np.mean(scos))

# all_f = ['Gender','country','Age','education','incomecategory','privacyinvasion','Deviceusage_smartphone','Deviceusage_desktop','Deviceusage_laptop','Deviceusage_tablet',
# 'Deviceusage_smartwatch','Deviceusage_mobilephone','falsifiedinformation','IUIPCCollection','IUIPCControl','IUIPCAwareness','Extraversion','Agreeableness','Conscientiousness','Neuroticism','Openness']
# all_f = ['Gender','Age','education','incomecategory','privacyinvasion','Deviceusage_smartphone','Deviceusage_desktop','Deviceusage_laptop','Deviceusage_tablet',
# 'Deviceusage_smartwatch','Deviceusage_mobilephone','falsifiedinformation','IUIPCCollection','IUIPCControl','IUIPCAwareness','Extraversion','Agreeableness','Conscientiousness','Neuroticism','Openness']
#
# #
# for f in all_f:
#     (X_train, y_train, X_test, y_test) = shuffleCSV(0.125, specTarget = 'sharelikeability_0', featuresToUse = ['country', f], random_state = 0)
#     X_train = np.asarray(X_train)
#     y_train = np.ravel(np.asarray(y_train))
#     X_test = np.asarray(X_test)
#     y_test = np.ravel(np.asarray(y_test))
#     score = RidgeRegr (X_train, y_train, X_test, y_test)
#     print("%s scored %.2f" % (f,score))
