import csv
import random
import json
from ui.models import Permissions, PermissionSettings
from ml.models import IUIPC, Baseline
from django.utils import timezone
from django.shortcuts import render, redirect

class Helper():
    def __init__(self):
        # To Do
        # print("Test")
        return

    def BinaryToBoolean(self, value):
        value = float(value)
        if (value < 3.5):
            return False
        else:
            return True

    def savePrediction(self, user, label, value):
        print("I would save for user %s the permission %s with the value %s " % (user, label, value))
        # If the permission was manually overwritten, do not do anything (e.g. user runs wizard a second time after changing stuff)
        label = self.parseFileName(label)
        # print(label)
        if self.checkIfManualOverride(user, label):
            return

        # Otherwise save changes to database
        # permObject = PermissionSettings.objects.get(userID=user, permID__permissionName__iexact=value)

        permObject = PermissionSettings.objects.filter(userID=user).get(permID__permissionName__iexact=label)
        # print(permObject)
        permObject.value = value
        permObject.lastchanged = timezone.now()
        permObject.save()
        return

    # This will set the initial permissions once the user logs in the first time
    def setInitialPermissions(self, user):
        TargetLabels = ['name', 'birthday', 'address', 'allergies', 'recent_shop_visits', 'nutrition_preferences', 'only_the_product_categories', 'household_income',
        'article_wishlist', 'device_location', 'products_as_on_receipt', 'recently_viewed_articles', 'loyalty_points', 'only_the_price', 'only_the_amount', 'in-store_location']
        for Target in TargetLabels:
            parsedTarget = self.parseFileName(Target)
            permission = Permissions.objects.get(permissionName__iexact=parsedTarget)
            try:
                obj = PermissionSettings.objects.get(permID=permission, userID=user)
                print("Object already existed, doing nothing")
            except PermissionSettings.DoesNotExist:
                obj = PermissionSettings(value=True, manual=False, permID=permission,userID=user)
                obj.save()


        # Lets set the initial iuipc now
        obj = IUIPC(UserID=user)
        obj.save()
        # And set baseline to false (default)
        try:
            base = Baseline(UserID=user)
            base.save()
        except:
            print("There already was a baseline setting for this user")


        return True

    # Looks for changes for the given json (Settings that the user changed) and will decide whether to set manual to True or False
    def lookForChanges(self, inputjson, user):
        parsedJSON = json.loads(inputjson)
        for item in parsedJSON:
            oldPerm = PermissionSettings.objects.get(id=item['id'])
            # Only update if the element has actually been actively changed by the user
            if not (oldPerm.value == item['value']):
                # Update in Database
                oldPerm.value = item['value']
                oldPerm.manual = True
                oldPerm.lastchanged = timezone.now()
                oldPerm.save()
                print("Updated %s in database" % str(oldPerm))
        return True

    # Saves a single IUIPC Result in the database
    def saveIUIPCResults(self, question, answer, user):
        print("I would save the question %s with the answer %s for the user %s" % (question, answer, str(user)))
        iui_old = IUIPC.objects.get(UserID=user)
        print(iui_old)
        setattr(iui_old, question, int(answer))
        print(iui_old)
        iui_old.save()
        return True

    # Parses an estimator file name (e.g. nutrition_preferences) to "Nutrition Preferences"
    def parseFileName(self, value):
        temp = value.replace("_"," ")
        return temp

    # Checks if the setting was manually overwritten, needs a user for the query
    def checkIfManualOverride(self, user, value):
        permObject = PermissionSettings.objects.get(userID=user, permID__permissionName__iexact=value)
        return permObject.manual

    # The Products as on the receipt permission will be set to True if all its 3 subcategories are true or set all subcategories to true of parent category is True
    def evaluateParentPermission(self, user, value):
        print("TODO")
