import csv
import random
import numpy as np




class Importer(object):

    def __init__(self):
        # To Do
        print("Test")

    def checkRow(self, row):
        sharelikeability_name =               row['sharelikeability_0']
        sharelikeability_birthday =               row['sharelikeability_1']
        sharelikeability_address =               row['sharelikeability_2']
        sharelikeability_allergies =               row['sharelikeability_3']
        sharelikeability_recent_shop_visits =               row['sharelikeability_4']
        sharelikeability_nutrition_preferences =               row['sharelikeability_5']
        sharelikeability_only_product_categories =               row['sharelikeability_6']
        sharelikeability_household_income =               row['sharelikeability_7']
        sharelikeability_article_wishlist =               row['sharelikeability_8']
        sharelikeability_location_device =               row['sharelikeability_9']
        sharelikeability_bought_products_receipt =               row['sharelikeability_10']
        sharelikeability_recently_viewed =               row['sharelikeability_11']
        sharelikeability_loyalty_points =               row['sharelikeability_12']
        sharelikeability_only_price =               row['sharelikeability_13']
        sharelikeability_only_amount =               row['sharelikeability_14']
        sharelikeability_location_store =               row['sharelikeability_15']

        targetitem = [sharelikeability_name,sharelikeability_birthday,sharelikeability_address,sharelikeability_allergies,sharelikeability_recent_shop_visits,sharelikeability_nutrition_preferences,
        sharelikeability_only_product_categories,sharelikeability_household_income,sharelikeability_article_wishlist,sharelikeability_location_device,sharelikeability_bought_products_receipt,
        sharelikeability_recently_viewed,sharelikeability_loyalty_points,sharelikeability_only_price,sharelikeability_only_amount,sharelikeability_location_store]

        targetitem = [item.replace(",", ".") for item in targetitem]
        targetitem =  [round(float(i),2) for i in targetitem]

        restrict = 0
        allow = 0
        for i in targetitem:
            if (i < 3.5):
                restrict = restrict + 1
            else:
                allow = allow + 1

        if (restrict == 0) or (allow == 0):
            # print(targetitem)
            # print("Trivial case")
            # print(restrict, allow)
            return True

        # Testing
        # if (restrict < 3) or (allow < 3):
        #     return True
        return False


    def readRow(self, row, Target, Features):
        Gender = row['Gender']
        Age =               row['Age']
        country =            row['country']
        education =               row['education']
        incomecategory =            row['incomecategory']
        privacyinvasion =               row['privacyinvasion']
        Deviceusage_smartphone =            row['Deviceusage_smartphone']
        Deviceusage_desktop =               row['Deviceusage_desktop']
        Deviceusage_laptop =            row['Deviceusage_laptop']
        Deviceusage_tablet =               row['Deviceusage_tablet']
        Deviceusage_smartwatch =            row['Deviceusage_smartwatch']
        Deviceusage_mobilephone =               row['Deviceusage_mobilephone']
        falsifiedinformation =            row['falsifiedinformation']
        IUIPCCollection =                        row['IUIPCCollection']
        IUIPCControl =                        row['IUIPCControl']
        IUIPCAwareness =                        row['IUIPCAwareness']
        bf_extraversion =                   row['Extraversion']
        bf_agreeableness =                   row['Agreeableness']
        bf_conscientiousness =                   row['Conscientiousness']
        bf_neuroticism =                   row['Neuroticism']
        bf_openness =                   row['Openness']
        sharelikeability_name =               row['sharelikeability_0']
        sharelikeability_birthday =               row['sharelikeability_1']
        sharelikeability_address =               row['sharelikeability_2']
        sharelikeability_allergies =               row['sharelikeability_3']
        sharelikeability_recent_shop_visits =               row['sharelikeability_4']
        sharelikeability_nutrition_preferences =               row['sharelikeability_5']
        sharelikeability_only_product_categories =               row['sharelikeability_6']
        sharelikeability_household_income =               row['sharelikeability_7']
        sharelikeability_article_wishlist =               row['sharelikeability_8']
        sharelikeability_location_device =               row['sharelikeability_9']
        sharelikeability_bought_products_receipt =               row['sharelikeability_10']
        sharelikeability_recently_viewed =               row['sharelikeability_11']
        sharelikeability_loyalty_points =               row['sharelikeability_12']
        sharelikeability_only_price =               row['sharelikeability_13']
        sharelikeability_only_amount =               row['sharelikeability_14']
        sharelikeability_location_store =               row['sharelikeability_15']

        # Check if trivial = 16 trivial cases in this data set
        if self.checkRow(row):
            return (None, None)

        if len(Features) >= 1:
            dataitem = []
            for feature in Features:
                dataitem.append(row[feature])
        else:
            dataitem = [Gender,Age,country,education,incomecategory,privacyinvasion,Deviceusage_smartphone,Deviceusage_desktop,Deviceusage_laptop,Deviceusage_tablet,Deviceusage_smartwatch,Deviceusage_mobilephone,
            falsifiedinformation,IUIPCCollection,IUIPCControl,IUIPCAwareness,bf_extraversion,bf_agreeableness,bf_conscientiousness,
            bf_neuroticism,bf_openness]

        if (Target != None):
            targetitem = [row[Target]]
        else:
            targetitem = [sharelikeability_name,sharelikeability_birthday,sharelikeability_address,sharelikeability_allergies,sharelikeability_recent_shop_visits,sharelikeability_nutrition_preferences,
            sharelikeability_only_product_categories,sharelikeability_household_income,sharelikeability_article_wishlist,sharelikeability_location_device,sharelikeability_bought_products_receipt,
            sharelikeability_recently_viewed,sharelikeability_loyalty_points,sharelikeability_only_price,sharelikeability_only_amount,sharelikeability_location_store]

        #Replace , with . to convert it to float later
        dataitem = [item.replace(",", ".") for item in dataitem]
        #Convert list to float
        dataitem =  [round(float(i),2) for i in dataitem]
        #Replace , with . to convert it to float later
        targetitem = [item.replace(",", ".") for item in targetitem]
        targetitem =  [round(float(i),2) for i in targetitem]

        # print(len(targetitem))
        # print(dataitem)

        return (dataitem, targetitem)
    def importCSV(self, specTarget, featuresToUse):
        # specTarget='sharelikeability_0'
        # featuresToUse=['IUIPCCollection','IUIPCControl','IUIPCAwareness']
        # Open the CSV file for input
        with open('learning_data.csv') as csvfile:
            dictreader = csv.DictReader(csvfile, delimiter=';', quotechar='|')

            data = []
            target = []
            for row in dictreader:
                (item_data,item_target) = self.readRow(row,specTarget,featuresToUse)
                if (item_data != None):
                            data.append(item_data)
                            target.append(item_target)

            data = np.asarray(data)
            target = np.ravel(np.asarray(target))
            return (data, target)
