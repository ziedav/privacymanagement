## Synopsis

This application is part of a master thesis and provides a privacy management including a privacy wizard to get the user started.

## Installation

You need the following dependencies (minor differences might work):
1. Python 3.4.5
2. Django 1.9.5
3. Numpy 1.11.1
4. scikit-learn 0.17.1
5. scipy 0.17.1

Just download the project and start it using "python manage.py runserver"

You might want to delete some of the sample data but be sure to keep the "Permissions"
